import Data.Array

data Tile = Peg | Hole | Unused deriving (Eq, Show)
data Direction = North | South | East | West deriving (Eq, Show)
type Board = Array (Integer, Integer) Tile
type Position = (Integer, Integer)
type Move = (Position, Direction)
type Path = [Move]
type Game = (Board, [Move])

iBoard = "           \
         \           \
         \    xxx    \
         \    xxx    \
         \  xxxxxxx  \
         \  xxx·xxx  \
         \  xxxxxxx  \
         \    xxx    \
         \    xxx    \
         \           \
         \           "

fBoard = "           \
         \           \
         \    ···    \
         \    ···    \
         \  ·······  \
         \  ···x···  \
         \  ·······  \
         \    ···    \
         \    ···    \
         \           \
         \           "

xBoard = "           \
         \           \
         \    ···    \
         \    ···    \
         \  ·······  \
         \  ·xx····  \
         \  ·······  \
         \    ···    \
         \    ···    \
         \           \
         \           "


tileToChar :: Tile -> Char
tileToChar tile 
    | tile == Peg    = 'x'
    | tile == Hole   = '·'
    | tile == Unused = ' ' 

charToTile :: Char -> Tile
charToTile char
    | char == ' '    = Unused
    | char == 'x'    = Peg
    | char == '·'    = Hole

printBoard :: Board -> IO()
printBoard board = putStr $ [ map tileToChar r | r <- rows] >>= (\x -> x ++"\n")
    where
        rows = [[board!(x,y)| x <-[0..10]] |  y <-[0..10]]

initialBoard :: Board
initialBoard = listArray ((0,0),(10,10)) $ map charToTile iBoard

finalBoard :: Board
finalBoard = listArray ((0,0),(10,10)) $ map charToTile fBoard

brokenBoard = listArray ((0,0),(10,10)) $ map charToTile xBoard

canMoveEast :: Position -> Board -> Bool
canMoveEast (x,y) board
    | (board!(x,y) == Peg) && (board!(x+1,y) == Peg) && (board!(x+2,y) == Hole) = True
    | otherwise  = False

canMoveWest :: Position -> Board -> Bool
canMoveWest (x,y) board
    | (board!(x,y) == Peg) && (board!(x-1,y) == Peg) && (board!(x-2,y) == Hole) = True
    | otherwise  = False

canMoveNorth :: Position -> Board -> Bool
canMoveNorth (x,y) board
    | (board!(x,y) == Peg) && (board!(x,y+1) == Peg) && (board!(x,y+2) == Hole) = True
    | otherwise  = False

canMoveSouth :: Position -> Board -> Bool
canMoveSouth (x,y) board
    | (board!(x,y) == Peg) && (board!(x,y-1) == Peg) && (board!(x,y-2) == Hole) = True
    | otherwise  = False

pegPositions :: Board -> [Position]
pegPositions board = [(x,y) |  x <- [2..10], y <- [2..10], board!(x,y) == Peg]

possibleMoves :: Board -> [Move]
possibleMoves board = eastMoves ++ westMoves ++ northMoves ++ southMoves 
    where 
        currentPegs = pegPositions board
        eastMoves = map (\x -> (fst x, East)) $ filter snd $ map (\x -> (x, canMoveEast x board) ) currentPegs
        westMoves = map (\x -> (fst x, West)) $ filter snd $ map (\x -> (x, canMoveWest x board) ) currentPegs
        northMoves = map (\x -> (fst x, North)) $ filter snd $ map (\x -> (x, canMoveNorth x board) ) currentPegs
        southMoves = map (\x -> (fst x, South)) $ filter snd $ map (\x -> (x, canMoveSouth x board) ) currentPegs

makeMove :: Board -> Move -> Board
makeMove board ((x,y),direction)
    | direction == North = board//[((x,y),Hole), ((x,y+1),Hole), ((x,y+2),Peg)]
    | direction == South = board//[((x,y),Hole), ((x,y-1),Hole), ((x,y-2),Peg)]
    | direction == East = board//[((x,y),Hole), ((x+1,y),Hole), ((x+2,y),Peg)]
    | direction == West = board//[((x,y),Hole), ((x-1,y),Hole), ((x-2,y),Peg)]

-- playGame winningBoard (board, moves)
--     | board == winningBoard = (board, moves)
--     | board /= winningBoard && possibleMoves == []  = (board, moves)
--     | otherwise = 

-- nextBoards boards = boards >>= \b -> map (makeMove b) $ possibleMoves b
--
playGame' :: Board -> Game -> Path 
playGame' winningBoard (board, path) 
   | board == winningBoard = path
   | board /= winningBoard && (possibleMoves board) == [] = []
   | otherwise = playGame' winningBoard (nextBoard, path ++ [move])
   where 
        move:remainingMoves = possibleMoves board
        nextBoard = makeMove board move
